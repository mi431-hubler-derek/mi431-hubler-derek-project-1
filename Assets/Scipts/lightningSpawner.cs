using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class lightningSpawner : MonoBehaviour
{
    UnityEvent mSpawnLightning;
    // Start is called before the first frame update
    void Start()
    {
        if (mSpawnLightning == null)
        {
            mSpawnLightning = new UnityEvent();
        }
        mSpawnLightning.AddListener(SpawnLightning);
    }

    // Update is called once per frame
    void Update()
    {
        int generateLightning = Random.RandomRange(0, 10);
        if (generateLightning <= 8)
        {
            Start();
        }
    }

    void SpawnLightning()
    {
        Debug.Log("LightningSpawned");
    }
}
